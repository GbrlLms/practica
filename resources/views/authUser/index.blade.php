@extends('layouts.base')

@section('body')


    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand" href="#" style="color: #e2e8f0">Practica</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/logout" style="color: #e2e8f0">Desloguearse</a>
                </li>


            </ul>

        </div>
    </nav>

    <div class="container">


        <div class="mt-5 mb-5">
            <div class="bg-info"><p> Estos son los logs de inicio de sesion para: <strong>{{ auth()->user()->username }}</strong></p></div>
            <input type="hidden" id="var" value="{{ auth()->user()->id }}">
        </div>

        <div class="row justify-content-center">

            <div class="row">
                <label for="tiempo" style="color: white; font-size: 1rem">Filtrar por dia</label>
                <input type="date" id="tiempo" value="{{ $date  }}">
            </div>

            <table class="table table-bordered table-striped  mt-5">
                <thead style="text-align: center; background: #a0aec0">
                <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Hora</th>
                </tr>
                </thead>
                <tbody style="background: slategrey" id="tbody">

                </tbody>
            </table>

        </div>
    </div>

    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script src="{{ asset('js/user.js') }}"></script>
    @endsection

@endsection
