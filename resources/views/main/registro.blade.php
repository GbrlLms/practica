@extends('layouts.base')

@section('body')

    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">

                        @if( session('errorR') )
                            <div class="alert-danger"><h3>Error con la informacion ingresada</h3></div>
                        @endif

                        <h5 class="card-title text-center">Registrar</h5>
                        <form method="post" class="form-signin" action="/registrar">
                            @csrf



                            <div class="form-label-group">
                                <input type="text"  class="form-control" name="nombre" placeholder="Nombre" >
                                @if( $errors->first('nombre') )
                                    <div class="alert-danger">{{ $errors->first('nombre') }}</div>
                                    @endif
                            </div>

                            <div class="form-label-group mt-3">
                                <input type="text"  class="form-control" name="username" placeholder="Usuario"  >
                                @if( $errors->first('username') )
                                    <div class="alert-danger">{{ $errors->first('username') }}</div>
                                @endif

                            </div>

                            <div class="form-label-group mt-3">
                                <input type="text"  class="form-control" name="email" placeholder="Correo"  >
                                @if( $errors->first('email') )
                                    <div class="alert-danger">{{ $errors->first('email') }}</div>
                                @endif
                            </div>

                            <div class="form-label-group mt-3">
                                <input type="password" name="password" class="form-control" placeholder="Password" >
                                @if( $errors->first('password') )
                                    <div class="alert-danger">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                            <button class="btn btn-lg  btn-block text-uppercase mt-5" id="btnRegistro" type="submit">Registrar</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
