@extends('layouts.base')

@section('body')


    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Logueo</h5>

                        @if( session('error') )
                            <div class="alert-danger"><h3>Error con la informacion ingresada</h3></div>
                        @endif

                        @if( session('registrado') )
                            <div class="alert-success"><h3>Registrado correctamente</h3></div>
                        @endif

                        <form class="form-signin" action="/verifylogin" method="post">
                            @csrf
                            <div class="form-label-group">
                                <input type="text"  class="form-control" name="username" placeholder="Usuario" required >
                                @if( $errors->first('username') )
                                    <div class="alert-danger">{{ $errors->first('username') }}</div>
                                @endif
                            </div>

                            <div class="form-label-group mt-3">
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                                @if( $errors->first('password') )
                                    <div class="alert-danger">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                            <button class="btn btn-lg  btn-block text-uppercase mt-5" id="btnLogueo" type="submit">Loguearse</button>
                            <a href="/registro" class="btn btn-lg  btn-block text-uppercase mt-5" id="btnRegistro">Registrar</a>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
