<?php

namespace App\Http\Controllers;

use App\Models\Sessiones;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{

    private $length = 60;
    private $token;
    public function __construct()
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

      $this->token = substr(str_shuffle(str_repeat($pool, 5)), 0, $this->length);
    }


    public function index(){
        return view('main.index');
    }

    public function verificarLogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ], [
            'username.required' => 'Es necesario un usuario',
            'password.required' => 'Es necesario un password',
        ]);


        $credenciales = ['username' => request('username'), 'password' => request('password')];

        if (Auth::attempt($credenciales)) {
            $request->session()->regenerate();
            $user = User::find(auth()->user()->id);
            $user->token = $this->token;
            $user->save();
            $session = new Sessiones;
            $session->user_id = auth()->user()->id;
            $session->save();
            return redirect('/user');
        } else {
            return redirect('/')->with([
                'error' =>'Valores incorrectos'
            ]);
        }
    }

    public function registro(){
        return view('main/registro');
    }

    public function registrar(Request $request){

        $request -> validate([
            'nombre' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ], [
            'nombre.required' => 'Es necesario un nombre',
            'username.required' => 'Es necesario un usuario',
            'username.unique' => 'Este usuario ya existe',
            'email.required' => 'Es necesario un correo electronico',
            'email.email' => 'Es necesario que ingreses un correo valido',
            'email.unique' => 'El correo electronico ya existe',
            'password.required' => 'Es necesario un password',

        ]);

        $nuevoUsuario = new User;
        $nuevoUsuario->name = $request->input('nombre');
        $nuevoUsuario->username = $request->input('username');
        $nuevoUsuario->email = $request->input('email');
        $nuevoUsuario->password = bcrypt($request->input('password'));


        if ($nuevoUsuario -> save()){

            return redirect('/')-> with(['registrado','Registrado correctamente']);
        }else{
            return redirect('/')-> with(['errorR','Hubo un problema al registrar el usuario, campos duplicados']);
        }


    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }


}
