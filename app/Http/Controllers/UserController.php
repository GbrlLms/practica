<?php

namespace App\Http\Controllers;
use App\Models\Sessiones;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $ldate = date('Y-m-d');
        return view('authuser.index')
            ->with('date', $ldate);
    }

    public function filtrarfecha($fecha, $info){
        $sesiones = Sessiones::where([
            ['user_id', '=',  $info],
            ['created_at', '=', $fecha]
        ])
            ->get();

        return response()->json([
            $sesiones
        ]);
    }

}
