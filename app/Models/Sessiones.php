<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sessiones extends Model
{
    use HasFactory;

    protected $table = 'sessiones';

    protected $fillable = [
      'user_id'
    ];

}
