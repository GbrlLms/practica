<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\MainController::class, 'index']);
Route::get('logout', [\App\Http\Controllers\MainController::class, 'logout'])->name('logout');
Route::post('verifylogin', [\App\Http\Controllers\MainController::class, 'verificarLogin']) ->name('verifylogin');
Route::get('registro', 'App\Http\Controllers\MainController@registro') -> name('registro');
Route::post('registrar', 'App\Http\Controllers\MainController@registrar') -> name('registrar');


Route::group(['prefix' => 'user'], function (){
    Route::get('', [\App\Http\Controllers\UserController::class, 'index']) -> name('user')->middleware('auth');
    Route::get('getitems/{fecha}/info/{info}', [\App\Http\Controllers\UserController::class, 'filtrarfecha']) -> name('getitems');
});




